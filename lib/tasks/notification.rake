namespace :notification do
  desc "Sends SMS notification to employees to remind them to log overtime"
  task sms: :environment do
    if Time.now.sunday?
      employees = Employee.all
      notification_msg = "Please log into the overtime dashboard to request or confirm overtime."
      ## For live product
      employees.each do |em|
        AuditLog.create!(user_id: em.id)
      #   SmsTool.send_sms(number, message)
      end
      ## For development and testing
      SmsTool.send_sms(6083205240, notification_msg)
    end
  end
  
  desc "Sends Mail notification to manager for pending overtime request"
  task manager_email: :environment do
    submitted_posts = Post.submitted
    admins = AdminUser.all
    if submitted_posts.count > 0
      admins.each do |admin|
        ManagerMailer.email(admin).deliver_now
      end
    end
  end
end
