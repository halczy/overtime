# Overtime App

## Keep track of employees overtime

## Models
- Post -> date:date rationale:text
- User -> Devise Gem
- AdminUser -> STI

## Features:
- Apprival Workflow
- SMS Sending
- Admin Dashboard
- Email Summary
- Employee No Overtime Documentation

## UI:
- Bootstrap
- Icons from glyphicons
- Update the styles for forms