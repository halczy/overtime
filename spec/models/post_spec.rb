require 'rails_helper'

RSpec.describe Post, type: :model do
  
  describe "Creation" do
    before do
      @post = FactoryGirl.create(:post)
    end
    
    it "can be created" do
      expect(@post).to be_valid
    end
    
    it "cannot be created without a date" do
      @post.date = nil
      expect(@post).not_to be_valid
    end
    
    it "can be created without filling in work performed" do
      @post.work_performed = nil
      expect(@post).to be_valid
    end
    
    it "cannot be created without daily_hours" do
      @post.daily_hours = nil
      expect(@post).not_to be_valid
    end
    
    it "has an daily_hours greater than 0.0" do
      @post.daily_hours = 0.0
      expect(@post).not_to be_valid
    end
  end

end
