require 'rails_helper'

RSpec.describe User, type: :model do
  
  before do
    @user = User.create(email: "test@test.com", 
                        phone: Faker::Number.number(10),
                        first_name: "John", last_name: "Doe", 
                        ssn: Faker::Number.number(4),
                        company: Faker::Company.name,
                        password: "example", password_confirmation: "example")
  end
  
  describe "creation" do
    it "can be created" do
      expect(@user).to be_valid
    end
  end
  
  describe "validation" do
    it "cannot be created without first_name" do
      @user.first_name = nil
      expect(@user).to_not be_valid
    end
    
    it "cannot be created without last_name" do
      @user.last_name = nil
      expect(@user).to_not be_valid
    end
    
    it "cannot be created without phone number" do
      @user.phone = nil
      expect(@user).to_not be_valid
    end
    
    it "requires the ssn attribute" do
      @user.ssn = nil
      expect(@user).to_not be_valid
    end
    
    it "requires a company" do
      @user.company = nil
      expect(@user).to_not be_valid
    end
    
    it "requires phone attribute to only contain integers" do
      @user.phone = "Thisisastr"
      expect(@user).to_not be_valid
    end
    
    it "requires phone attribute to only have 10 characters" do
      @user.phone = "11234567890"
      expect(@user).to_not be_valid
    end
  end

  describe "custom name methods" do
    it "has a full_name method that combines first and last name" do
      expect(@user.full_name).to eq("DOE, JOHN")
    end
  end
  
  describe 'relationship between admin and employee' do
    it 'allows admin to be associated with multiple employee' do
      employee_1 = FactoryGirl.create(:user)
      employee_2 = FactoryGirl.create(:user)
      admin = FactoryGirl.create(:admin_user)
      Hand.create!(user_id: admin.id, hand_id: employee_1.id)
      Hand.create!(user_id: admin.id, hand_id: employee_2.id)
      
      expect(admin.hands.count).to eq(2)
    end
  end
end
