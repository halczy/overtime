require 'rails_helper'

RSpec.describe AuditLog, type: :model do
  before do
    @audit_log = FactoryGirl.create(:audit_log)
  end
  
  describe 'creation' do
    it "can be created" do
      expect(@audit_log).to be_valid
    end
  end
  
  describe 'validation' do
    it "should have a user association" do
      @audit_log.user_id = nil
      expect(@audit_log).not_to be_valid
    end
    
    it "should always have a status" do
      @audit_log.status = nil
      expect(@audit_log).not_to be_valid
    end
    
    it "should be require to have a start date" do
      @audit_log.start_date = nil
      expect(@audit_log).not_to be_valid
    end
    
    it "should have a start date equal to 6 days ago" do
      new_audit = AuditLog.create(user_id: User.last.id, status: 0)
      expect(new_audit.start_date).to eq(new_audit.created_at.to_date - 6.days)
    end
    
    
  end
end
