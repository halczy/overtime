FactoryGirl.define do
  
  sequence :work_performed do |n|
    "This is work ##{n}"
  end
  
  factory :post do
    user
    date Date.today
    work_performed { generate :work_performed }
    daily_hours 12.5
  end
  
end