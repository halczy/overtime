FactoryGirl.define do
  
  sequence :email do |n|
    "test#{n}@example.com"
  end
  
  factory :user, class: "Employee" do
    first_name "John"
    last_name "Doe"
    email { generate :email} 
    phone Faker::Number.number(10)
    ssn Faker::Number.number(4)
    company Faker::Company.name
    password "example"
    password_confirmation "example"
  end
  
  factory :admin_user, class: "AdminUser" do
    first_name "Admin"
    last_name "User"
    phone Faker::Number.number(10)
    ssn Faker::Number.number(4)
    company Faker::Company.name
    email { generate :email }
    password "example"
    password_confirmation "example"
  end

end