require 'rails_helper'

describe 'access_restriction' do
  
  it "can be reached by admin" do
    admin = FactoryGirl.create(:admin_user)
    login_as(admin, scope: :user)
    visit admin_root_path
    
    expect(page.current_path).to eq(admin_root_path)
  end
  
  it "cannot be reach by regular user" do
    user = FactoryGirl.create(:user)
    login_as(user, scope: :user)
    visit admin_root_path
    
    expect(page.current_path).to eq(root_path)
  end
  
  it "cannot be reach by visitor" do
    visit admin_root_path
    
    expect(page.current_path).to eq(new_user_session_path)
  end
end