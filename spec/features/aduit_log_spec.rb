require 'rails_helper'

describe 'AuditLog' do

  before :each do
    FactoryGirl.create(:audit_log)
    admin = FactoryGirl.create(:admin_user)
    login_as(admin, scope: :user)
  end
  
  describe 'index' do
    it 'has an index page that can be reach (by admin)' do
      visit audit_logs_path
      expect(page.current_path).to eq audit_logs_path
    end
    
    it 'renders audit log content' do
      visit audit_logs_path
      expect(page).to have_content(/DOE/)
    end
    
    it 'cannot be accessed by non-admin user' do
      user = FactoryGirl.create(:user)
      logout(:user)
      login_as(user, scope: :user)
      visit audit_logs_path

      expect(page.current_path).to eq root_path
    end
  end
end