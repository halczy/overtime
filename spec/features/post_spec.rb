require 'rails_helper'

describe 'navigate' do
  
  let(:user) { FactoryGirl.create(:user) }
  
  before do
    login_as(user, :scope => :user)
    Post.create(date: Date.yesterday, work_performed: "Post1", user_id: user.id, daily_hours: 1.5)
    Post.create(date: Date.yesterday, work_performed: "Post2", user_id: user.id, daily_hours: 2)
    @post_edit = Post.create(date: Date.yesterday, 
                             work_performed: "Post E", 
                             user_id: user.id,
                             daily_hours: 1.2)
    @post_destroy = Post.create(date: Date.yesterday, 
                                work_performed: "Post D", 
                                user_id: user.id,
                                daily_hours: 2.2)
    @post_approve = Post.create(date: Date.yesterday, 
                                work_performed: "Post A", 
                                user_id: user.id, 
                                daily_hours: 3.2, 
                                status: 1)
  end

  describe 'index' do
    before do
      visit posts_path
    end
    
    it 'can be reached successfully' do
      expect(page.status_code).to eq(200)
    end
    
    it 'has a title of Posts' do
      expect(page).to have_content(/Entries/)
    end
    
    it "has a list of posts" do
      visit posts_path
      
      expect(page).to have_content(/Post1|Post2/)
    end
    
    it 'scopes posts listing to only post author' do
      FactoryGirl.create(:post)
      
      visit posts_path
      expect(page).not_to have_content(/This is work/)
      
    end
  end

  describe 'new' do
    it "has a link fromt the homepage" do
      visit root_path
      click_link "new_post_from_nav"
      
      expect(page.status_code).to eq(200)
    end
  end
  
  describe 'creation' do
    before do
      visit new_post_path
    end
    
    it "has a new form that can be reached" do
      expect(page.status_code).to eq(200)
    end
    
    it "can be created from new form page" do
      fill_in 'post[date]', with: Date.today
      fill_in 'post[work_performed]', with: "Some work"
      fill_in 'post[daily_hours]', with: 2.7
      
      expect { click_on "Save" }.to change(Post, :count).by(1)
      expect(page).to have_content("Some work")
    end
    
    it "will have user associated with it" do
      fill_in 'post[date]', with: Date.today
      fill_in 'post[work_performed]', with: "User_Association"
      fill_in 'post[daily_hours]', with: 1.0
      click_on "Save"
      
      expect(User.last.posts.last.work_performed).to eq("User_Association")
    end
  end
  
  describe 'edit' do
    
    it "can be edited" do
      visit edit_post_path(@post_edit)
      fill_in 'post[date]', with: Date.today
      fill_in 'post[work_performed]', with: 'Edited content'
      click_on "Save"
      
      expect(page).to have_content("Edited content")
    end
    
    it "cannot be edited by a non authorized user" do
      logout(:user)
      non_auth_user = FactoryGirl.create(:user)
      login_as(non_auth_user, scope: :user)
      
      visit edit_post_path(@post_edit)
      expect(page.current_path).to eq(root_path)
    end
    
    it "should not show edit link if post is approved" do
      visit posts_path

      expect(page).not_to have_css("#edit_#{@post_approve.id}")
    end
  end
  
  describe 'delete' do
    it "can be deleted" do
      visit posts_path
      click_link "delete_post_#{@post_destroy.id}_from_index"

      expect(page.status_code).to eq(200)
    end
  end
end