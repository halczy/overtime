@employee = Employee.create(email: "employee@seed.com", 
                    phone: Faker::Number.number(10),
                    first_name: "Mike", last_name: "Seed-User",
                    ssn: Faker::Number.number(4),
                    company: Faker::Company.name,
                    password: "example", password_confirmation: "example")
puts "Employee created."

AdminUser.create(email:"admin@seed.com", 
                 phone: Faker::Number.number(10),
                 first_name: "John", last_name: "Admin",
                 ssn: Faker::Number.number(4),
                 company: Faker::Company.name,
                 password: "example", password_confirmation: "example")
puts "Admin created."                 
                 
AuditLog.create(user_id: @employee.id, status: 0, start_date: (Date.today - 6.days))
AuditLog.create(user_id: @employee.id, status: 0, start_date: (Date.today - 13.days))
AuditLog.create(user_id: @employee.id, status: 0, start_date: (Date.today - 20.days))
puts "3 audit log has been created"

10.times do |n|
  Post.create(date: Date.today, 
              work_performed: Faker::Lorem.paragraph, 
              user_id: @employee.id,
              daily_hours: 12.5)
end
puts "#{Post.count} posts has been created."

