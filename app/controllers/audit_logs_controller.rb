class AuditLogsController < ApplicationController
  # Filter
  before_action :set_audit_log, only: [:confirm]
  
  def index
    @audit_logs = AuditLog.all.order(:id).page(params[:page]).per(10)
    
    authorize @audit_logs
  end
  
  def confirm
    authorize @audit_log
    
    @audit_log.confirmed!
    redirect_to root_path, notice: "This audit has been confirmed!"
  end

  private
  
    def set_audit_log
      @audit_log = AuditLog.find(params[:id])
    end
end
