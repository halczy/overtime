class AuditLogPolicy < ApplicationPolicy

  def index?
    user.admin?
  end
  
  def confirm?
    user.id == record.user_id
  end

end