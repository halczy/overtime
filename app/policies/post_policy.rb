class PostPolicy < ApplicationPolicy

  def update?
    return true if post_approved? && user.admin?
    return true if ( own_post? || user.admin? ) && !post_approved?
  end
  
  def approve?
    user.admin?
  end
  
  private
  
    def own_post?
      record.user_id == user.id
    end
    
    def post_approved?
      record.approved?
    end
end