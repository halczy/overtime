class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Relationships
  has_many :posts
  has_many :audit_logs
  has_many :hands_associations, class_name: "Hand"
  has_many :hands, through: :hands_associations
  
  # Validations
  PHONE_REGEX = /\A[0-9]*\z/
  validates_presence_of :first_name, :last_name, :phone, :ssn, :company
  validates :phone, length: { is: 10 }, format: PHONE_REGEX
  validates :ssn, length: { is: 4}, numericality: { only_integer: true }
  
  def full_name
    "#{last_name.upcase}, #{first_name.upcase}"
  end
  
  def employee?
    self.is_a?(Employee)
  end
  
  def admin?
    User.admin_types.include?(self.class.to_s)
  end
  
  def self.admin_types
    ['AdminUser']
  end
end