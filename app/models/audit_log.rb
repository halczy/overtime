class AuditLog < ApplicationRecord
  # Relationships
  belongs_to :user
  
  # Validations
  validates_presence_of :status, :start_date
  
  # Callbacks
  after_initialize :set_defaults
  before_update :set_end_date, if: :confirmed?
  
  # Enum
  enum status: { pending: 0, confirmed: 1}
  
  # Scope
  scope :by_start_date, -> { order(start_date: :asc) }
  
  private
  
    def set_defaults
      self.start_date ||= (Date.today - 6.days)
    end
    
    def set_end_date
      self.end_date =  Date.today
    end
end
