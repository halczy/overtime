class Post < ApplicationRecord
  # Relationships
  belongs_to :user
  
  # Validations
  validates_presence_of :date, :daily_hours
  validates :daily_hours, numericality: { greater_than: 0 }
  
  # Enum
  enum status: { submitted: 0, approved: 1, rejected: 2 }
  
  # Scope
  scope :posts_by, ->(user) { where(user_id: user.id) }

  # Filter
  after_save :confirm_audit_log, if: :submitted?
  after_save :reset_audit_log, if: :rejected? 
  
  private
    
    def confirm_audit_log
      audit_log = AuditLog.where(user_id: self.user_id, 
                                 start_date: (self.date - 7.days..self.date)).last
      audit_log.confirmed! if audit_log
    end
    
    def reset_audit_log
      audit_log = AuditLog.where(user_id: self.user_id, 
                                 start_date: (self.date - 7.days..self.date)).last
      audit_log.pending! if audit_log
    end
end
