class ApplicationMailer < ActionMailer::Base
  default from: 'OmicronPlus Overtime <no-reply@omicronplus.com>'
  layout 'mailer'
end
