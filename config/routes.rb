Rails.application.routes.draw do


  devise_for :users, skip: [:registrations]

  root 'static#homepage'

  resources :posts do
    member do
      patch :approve
    end
  end
  resources :audit_logs, except: [:new, :edit, :destroy] do
    member do
      patch :confirm
    end
  end
  
  namespace :admin do
    resources :users
    resources :posts
    resources :admin_users
    resources :employees

    root to: "users#index"
  end

end
